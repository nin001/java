// 1 4 9 
// 16 25 36
// 49 64 81

class ForDemo{
	public static void main(String a[]){
		int row = 3,col = 3;
		int var = 1;
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=col ; j++){
				System.out.print(var*var++ + "\t");
			}
			System.out.println();
		}
	}
}
