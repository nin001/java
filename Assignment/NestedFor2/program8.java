// A b C d
// E f G h
// I j K l
// M n O p

class ForDemo{
	public static void main(String a[]){
		int row = 4,col = 4;
		char ch1 = 'A';
		char ch2 = 'a';

		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=col ; j++){
				if(j%2 == 0){
					System.out.print(ch2 + " ");
				}else{
					System.out.print(ch1 + " ");
				}
				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}
