// 1 2 9 
// 4 25 6 
// 49 8 81

class ForDemo{
	public static void main(String a[]){
		int row = 3,col = 3;
		int var = 1;
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=col ; j++){
				if(var%2 == 0)
					System.out.print(var + " ");
				else
					System.out.print(var*var + " ");
				var++;
			}
			System.out.println();
		}
	}
}
