// 9 8 7
// 9 8 7
// 9 8 7
class ForDemo{
	public static void main(String a[]){
		int row = 3,col = 3;
		for(int i = 1 ; i<=row ; i++){
			int var = 9;
			for(int j = 1 ; j<=col ; j++){
				System.out.print(var-- + " ");
			}
			System.out.println();
		}
	}
}
