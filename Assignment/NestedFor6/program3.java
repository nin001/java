// perfect square

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Start : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		for(int i = start ; i*i<=end ; i++){
			System.out.print(i*i + " ");
		}
		System.out.println();
	}
}
