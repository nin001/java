//numbers divisible by 5 and that are even and also print the count

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter lower limit : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter Upper Limit : ");
		int end = Integer.parseInt(br.readLine());

		int count = 0;
		System.out.println("Output :");
		for(int i = start ; i<=end ; i++){
			if(i%5 == 0 && i%2 == 0){
				count++;
				System.out.print(i + " ");
			}
		}
		System.out.println("\nCount : "+count);
	}
}
