// perfect numbers in range

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		System.out.println("Perfect Number : ");
		for(int i = start ; i<=end ; i++){
			int sum = 0;
			for(int j = 1 ; j<i ; j++){
				if(i%j == 0){
					sum += j;
				}
			}

			if(sum == i){
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}
}
