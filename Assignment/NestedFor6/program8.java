// Palindorme numbers in range

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());
		System.out.println("Palindrome in range : ");
		for(int i = start ; i<=end ; i++){
			int num = i;
			int pal = 0;
			for(int temp = num ; temp!=0 ; temp/=10){
				pal = pal*10 + temp%10;
			}

			if(pal == num)
				System.out.print(pal + " ");
		}
		System.out.println();
	}
}
