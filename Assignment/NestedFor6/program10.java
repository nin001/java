// Palindorme numbers in range

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		System.out.println("Armstrong number in range : ");
		for(int i = start ; i<=end ; i++) {
			int digCount = 0;
			int temp = i;
			while(temp!=0){
				digCount++;
				temp /= 10;
			}

			temp = i;
			int arms = 0;
			while(temp!=0){
				int prod = 1;
				for(int j = 1 ; j<=digCount ;j++){
					prod *= temp%10;
				}
				arms += prod;
				temp /=10;
			}
			if(arms == i)
				System.out.print(arms + " ");

		}
		System.out.println();
	}
}
