// count vowels and constants in a string


import java.io.*;

class Demo {

	static int myStringLen(String str) {
		char arr[] = str.toCharArray();

		int len = 0;

		for(char c : arr)
			len++;

		return len;
	}

	static void countAlphabets(String str) {
		int len = myStringLen(str);

		int itr = 0;
		int vCount = 0;
		int cCount = 0;

		while(itr < len) {
			if((str.charAt(itr) == 'a')||(str.charAt(itr) == 'e')||(str.charAt(itr) == 'i')||(str.charAt(itr) == 'o')||(str.charAt(itr) == 'u'))
				vCount++;
			else
				cCount++;

			itr++;
		}

		System.out.println("Vowels in String "+str+" : "+vCount);
		System.out.println("Constants in String "+str+" : "+cCount);
	}
	public static void main(String args[] ) throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String ; ");
		String str = input.readLine();

		countAlphabets(str);
	}
}
