// Palindrom string

import java.io.*;

class Demo {

	static int myStringLength(String str1) {
	
		int len = 0;
		char arr[] = str1.toCharArray();

		for(char a : arr)
			len++;

		return len;

	}
	static boolean isPalString(String str1) {
		int len = myStringLength(str1);

		int itr1 = 0;
		int itr2 = len-1;

		while(itr1<itr2) {
			if(str1.charAt(itr1) != str1.charAt(itr2))
				return false;
			itr1++;
			itr2--;
		}

		return true; 
	}

	public static void main(String args[]) throws IOException {
		BufferedReader input = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter String : ");
		String str = input.readLine();

		if(isPalString(str)) {
			System.out.println("String "+str+" is Palindrome string");
		}else{
			System.out.println("String "+str+" is not Palindrome string");
		}

	}
}
