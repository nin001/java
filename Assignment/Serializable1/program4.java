// print square roots

import java.util.Scanner;

class Demo {

	static void printSquareRoot(int start , int end) {
		System.out.print("Perfect Square roots : ");
		for(int i = start ; i*i<=end ; i++) {
			System.out.print((i*i)+ " ");
		}
		System.out.println();
	}		
	public static void main(String args[] ){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter start : ");
		int start = input.nextInt();

		System.out.println("Enter end : ");
		int end = input.nextInt();

		if(start>end){
			System.out.println("Invalid Data\nReenter Start End ");
			main(args);
		}else{
			printSquareRoot(start,end);
		}
	}
}
