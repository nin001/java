// Trim string


import java.io.*;

class Demo {

	static int myStringLen(String str) {
		int len = 0;
		char arr[] = str.toCharArray();

		for(char c : arr)
			len++;

		return len;
	}

	static String trimStringDemo(String str) {
		int len = myStringLen(str);
		String retString = "";
		int flag = 2;
		for(int i = 0 ; i<len ; i++) {
			if(i == 0 && str.charAt(i) == ' ')
				flag = 0;

			if(str.charAt(i) !=' '){
				flag = 1;
			}
			if(flag == 1){
				if(str.charAt(i) == ' '){
					if(i == 0 || i== len-1){
						continue;
					}else{
						if(str.charAt(i+1) != ' ')
							retString = retString + str.charAt(i);
					}
				}else{
					retString = retString + str.charAt(i);
				}
			}
		}

		return retString;

	}
	public static void main(String args[] ) throws IOException {
		BufferedReader input = new BufferedReader (new InputStreamReader(System.in));

		System.out.println("Enter string : ");
		String str = input.readLine();

		System.out.println("Before trim : "+str);
		str = trimStringDemo(str);
		System.out.println("After trim : "+str);
	}
}
