// print char if same or print their diff

import java.io.*;

class ForDemo{
	public static void main(String args[])throws IOException{
		InputStreamReader input = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(input);
		System.out.println("Enter character 1 : ");
		char ch1 = (char)(input.read());
		
		br.skip(1);
		System.out.println("Enter character 2 : ");
		char ch2 = (char)(input.read());

		if(ch1 == ch2){
			System.out.println("Characters are same : "+ch1);
		}else{
			if(ch1>ch2){
				System.out.println("Character Difference in "+ch1+" and "+ch2+" is : "+(ch1-ch2));
			}else{
				System.out.println("Character Difference in "+ch2+" and "+ch1+" is : "+(ch2-ch1));
			}
		}
	}
}
