// even numbers in reverse order and odd numbers as it is in given range from user

import java.util.Scanner;

class ForDemo{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter start : ");
		int start = sc.nextInt();

		System.out.println("Enter End : ");
		int end = sc.nextInt();

		for(int i = end ; i>=start ; i--){
			if(i%2 == 0)
				System.out.print(i + " ");
		}
		System.out.println();
		for(int i = start ; i<=end ; i++){
			if(i%2 != 0)
				System.out.print(i + " ");
		}
		System.out.println();
	}
}
