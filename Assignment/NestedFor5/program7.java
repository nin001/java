/*10
  IH
  765
  DCBA*/

import java.util.Scanner;

class ForDemo{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row : ");
		int row = sc.nextInt();

		int num = (row*(row+1))/2;
		char ch = (char)(64+num);

		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(row%2==0){
					if(i%2==0){
						System.out.print(ch + " ");
					}else{
						System.out.print(num + " ");
					}
				}else{
					if(i%2 == 0){
						System.out.print(num + " ");
					}else{
						System.out.print(ch + " ");
					}
				}
				num--;
				ch--;
			}
			System.out.println();
		}
	}
}
