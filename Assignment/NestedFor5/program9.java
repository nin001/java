// factorial addition of each digit

import java.util.Scanner;

class ForDemo{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter number : ");
		int num = input.nextInt();

		int factSum = 0;
		while(num!=0){
			int fact = 1;
			for(int i = 1 ; i<=num%10 ; i++){
				fact *= i;
			}
			factSum += fact;
			num /=10;
		}
		System.out.println("Factorial addition of each Digit : "+factSum);
	}
}
