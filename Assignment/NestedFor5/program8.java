/*
 $
 @@
 &&&
 ####
*/

import java.io.*;

class ForDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());

		int flag = 1;

		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(flag == 1){
					System.out.print("$ ");
				}else if(flag == 2){
					System.out.print("@ ");
				}else if(flag == 3){
					System.out.print("& ");
				}else{
					System.out.print("# ");
				}
			}
			System.out.println();
			flag++;

			if(flag>4)
				flag = 1;
		}
	}
}
