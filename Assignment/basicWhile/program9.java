// palindrome number
class While{
	public static void main(String a[]){
		int num = 1221;
		int temp = num;
		int rev = 0;

		while(temp!=0){
			rev = rev*10 + temp%10;
			temp /= 10;
		}

		if(rev == num)
			System.out.println(rev + " is a palindrome");
		else
			System.out.println(rev + " is not a palindrome");
	}
}

