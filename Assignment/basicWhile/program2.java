// Factorial;
class While{
	public static void main(String a[]){
		int num = 6;
		int fact = 1;
		while(num>1){
			fact *= num--;
		}

		System.out.println(fact);
	}
}
