// count odd digits in number
class While{
	public static void main(String a[]){
		int num = 1245678;
		int count = 0;
		while(num!=0){
			if((num%10)%2 != 0)
				count++;
			num = num/10;
		}

		System.out.println(count);
	}
}
