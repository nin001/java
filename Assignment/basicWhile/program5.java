// print square of even digits of the number
class While{
	public static void main(String a[]){
		int num = 1234567;
		while(num!=0){
			if((num&10)%2 == 0)
				System.out.print((num%10)*(num%10) + " ");
			num /= 10;
		}
		System.out.println();
	}
}
