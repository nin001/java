// sum of even and multiplication of odd of digits
class While{
	public static void main(String a[]){
		int num = 135756;
		int sum = 0;
		int mult = 1;
		while(num!=0){
			if((num%10)%2 == 0)
				sum += num%10;
			else
				mult *= num%10;
			num /= 10;
		}

		System.out.println("Sum of even digits " + sum);
		System.out.println("Mult of odd digits " + mult);
	}
}
