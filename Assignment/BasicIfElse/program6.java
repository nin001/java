// print the number of days according to the month entered

class Basic{
	public static void main(String a[]){
		int month = 11;

		if(month == 1)
			System.out.println("January has 31days");
		else if(month == 2)
			System.out.println("February has 28days");
		else if(month == 3)
			System.out.println("March has 31days");
		else if(month == 4)
			System.out.println("April has 30days");
		else if(month == 5)
			System.out.println("May has 31days");
		else if(month == 6)
			System.out.println("June has 30days");
		else if(month == 7)
			System.out.println("July has 31days");
		else if(month == 8)
			System.out.println("August has 31days");
		else if(month == 9)
			System.out.println("September has 30days");
		else if(month == 10)
			System.out.println("October has 31days");
		else if(month == 11)
			System.out.println("November has 30days");
		else if(month == 12)
			System.out.println("December has 31days");
		else
			System.out.println("Invalid month");
	}
}
