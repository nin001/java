// find profit loss
class Basic{
	public static void main(String a[]){
		int bPrice = 1200;
		int sPrice = 1000;

		if(bPrice<sPrice){
			System.out.println("Profit of " + (sPrice-bPrice));
		}else if(bPrice>sPrice){
			System.out.println("Loss of " + (sPrice-bPrice));
		}else{
			System.out.println("No profit no loss");
		}
	}
}
