/*Program 10
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123
*/

import java.io.*;

class ArrayDemo{
	void EvenDigSum(int arr[]){
		
		for(int i = 0 ; i<arr.length ; i++){
			int sum = 0;
			int num = arr[i];
			while(num!=0){
				sum += num%10;
				num /= 10;
			}
			if(sum%2 == 0)
				System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("enter array elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		ArrayDemo obj = new ArrayDemo();

		obj.EvenDigSum(arr);
	}
}
