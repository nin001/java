/*Program 7
WAP to find the common elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Common elements :
1
2
*/
import java.io.*;

class ArrayDemo{
	static void UncommonEle(int arr1[] , int arr2[]){
		System.out.println("UnCommon Element :");
		for(int i = 0 ; i<arr1.length ; i++){
			int flag = 0;
			for(int j = 0 ; j<arr2.length ; j++){
				if(arr1[i] == arr2[j]){
					flag = 1;
				}
			}
			if(flag == 0)
				System.out.print(arr1[i]+ " ");
		}
		for(int i = 0 ; i<arr2.length ; i++){
			int flag = 0;
			for(int j = 0 ; j<arr1.length ; j++){
				if(arr1[i] == arr2[j]){
					flag = 1;
				}
			}
			if(flag == 0)
				System.out.print(arr2[i]+" ");
		}
		System.out.println();
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr1[] = new int[n];
		int arr2[] = new int[n];
		System.out.println("Enter array 1 element :");
		for(int i = 0 ; i<arr1.length ; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter array 2 elements :");
		for(int i = 0 ; i<arr2.length ; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		UncommonEle(arr1,arr2);
	}
}
	
