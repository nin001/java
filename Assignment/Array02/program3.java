/*Program 3
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
*/
import java.io.*;

class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size : ");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		int evenSum = 0, oddSum = 0;
		System.out.println("Enter Array elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0)
				evenSum+=arr[i];
			else
				oddSum+=arr[i];
		}

		System.out.println("Sum of Even Elements : "+evenSum);
		System.out.println("Sum of Odd Elements : "+oddSum);
	}
}
