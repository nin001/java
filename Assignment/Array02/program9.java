/*Program 9
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array
*/

import java.io.*;

class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size 1:");
		int n1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[n1];
		
		System.out.println("Enter size 2:");
		int n2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[n2];
		
		int arr3[] = new int[n1+n2];
		int itr = 0;
		System.out.println("Enter Array1 Elements : ");
		for(int i = 0 ; i<arr1.length ; i++){
			arr1[i] = Integer.parseInt(br.readLine());
			arr3[itr++] = arr1[i];
		}

		System.out.println("Enter Array2 Elements : ");
		for(int i = 0 ; i<arr2.length ; i++){
			arr2[i] = Integer.parseInt(br.readLine());
			arr3[itr++] = arr2[i];
		}

		System.out.println("Merged Array :");	
		for(int i = 0 ; i<n1+n2 ; i++){
			System.out.println(arr3[i] + " ");
		}
	}
}
