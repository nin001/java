/*Program 5
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0
*/
import java.io.*;

class ArrayDemo{
	int minimumElement(int arr[]){
		int min = arr[0];
		for(int i = 1 ; i<arr.length ; i++){
			if(arr[i]<min)
				min = arr[i];
		}
		return min;
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());
		
		int arr[] = new int[n];
		System.out.println("Enter element :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		ArrayDemo obj = new ArrayDemo();
		System.out.println("Min element : "+obj.minimumElement(arr));
	}
}
