/*
6 
5 4 
3 2 1
*/

class ForDemo{
	public static void main(String a[]){
		int var = 6;
		for(int i = 1 ; i<=3 ; i++){
			for(int j = 1 ; j<=i ; j++){
				System.out.print(var-- + " ");
			}
			System.out.println();
		}
	}
}
