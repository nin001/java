/*
F
EF
DEF
CDEF
BCDEF
ABCDEF
*/

class ForDemo{
	public static void main(String a[]){
		int row = 6,col = 6;
		char ch1 = (char)(64+row);
		for(int i = 1 ; i<=row ; i++){
			char ch2 = ch1;
			for(int j = 1 ; j<=i ; j++){
				System.out.print(ch2++ + " ");
			}
			System.out.println();
			ch1--;
		}
	}
}
