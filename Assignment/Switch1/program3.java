/*Write a program in which user should enter two numbers if both the numbers are positive
multiply them and provide to switch case to verify number is even or odd, if user enters any
negative number program should terminate saying “Sorry negative numbers not allowed”*/

import java.io.*;

class SwitchDemo{
	public static void main(String args[])throws IOException{
		BufferedReader Input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number : ");
		int a = Integer.parseInt(Input.readLine());

		System.out.println("Enter number : ");
		int b = Integer.parseInt(Input.readLine());
		int mult = 1;
		if(a<0 || b<0)
			mult = -1;
		else
			mult = (a*b)%2;
		switch(mult){
			case 0:
				System.out.println("Even");
				break;
			case 1:
				System.out.println("Odd");
				break;
			default:
				System.out.println("Sorry negative numbers are not allowed");
		}
	}
}
