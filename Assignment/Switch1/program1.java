// wap in which student should enter marks of five subject 
// if all subject have passing marks add them and print grades (first class / sec class) 
// using switch case

import java.io.*;

class SwitchDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int flag = 1;
		int sum = 0;
		for(int i = 1 ; i<=5 ; i++){
			System.out.println("Enter marks : ");
			int marks = Integer.parseInt(br.readLine());
			if(marks<33)
				flag = 0;
			sum += marks;
			if(flag == 0){
				sum = 0;
			}
		}
		
		sum = (sum/5);
		int var = 0;
		
		if(sum>70)
			var = 1;
		else if(sum<=70 && sum >50)
			var = 2;
		switch(var){
			case 1:
			       System.out.println("First class");
			       break;
		 	case 2:
			       System.out.println("Second class");
			       break;
			default:
			       System.out.println("No class");
			       break;
		}
	}
}
			
			
