// F
// E 1
// D 2 E
// C 3 D 4
// B 5 C 6 D
// A 7 B 8 C 9

class ForDemo{
	public static void main(String a[]){
		int row = 6;

		char ch = (char)(64+row);

		int num = 1;

		for(int i = 1 ; i<=row ; i++){
			char ch2 = ch;
			for(int j = 1 ; j<=i ; j++){
				if(j%2 == 0)
					System.out.print(num++ + " ");
				else
					System.out.print(ch2++ + " ");
			}
			System.out.println();
			ch--;
		}
	}
}
