//A B C D
//B C D
//C D
//D
class ForDemo{
	public static void main(String a[]){
		int row = 4;
		for(int i = 1 ; i<=row ; i++){
			char var = (char)(64+i);
			for(int j = 1 ; j<=row-i+1 ; j++){
				System.out.print(var++ + " ");
			}	
			System.out.println();
		}
	}
}
