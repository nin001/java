// 1
// 3 4 
// 6 7 8 

class ForDemo{
	public static void main(String a[]){
		int row = 4;
		int num = 1;
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(i==j){
					System.out.print(num++ + " ");
					num++;
				}else{
					System.out.print(num++ + " ");
				}
			}
			System.out.println();
		}
	}
}
