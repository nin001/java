/*
 Program 3
WAP to find a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4
*/

import java.io.*;

class ArrayDemo{

	static int CompositeIndex(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int count = 0;
			for(int j = 1 ; j*j<arr[i] ; j++){
				if(arr[i]%j == 0)
					count++;
			}
			if(count>=2)
				return i;
		}
		return -1;
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size : ");
		int n = Integer.parseInt(br.readLine());
		
		int arr[] = new int[n];
		System.out.println("Enter array elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret = CompositeIndex(arr);
		if(ret != -1){
			System.out.println("Composite number "+arr[ret]+" present at index : "+ret);
		}else{
			System.out.println("Composite number not present");
		}
	}
}

