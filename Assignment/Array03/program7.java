/*Program 7
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5*/
import java.io.*;

class ArrayDemo{

	static int StrongNo(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int temp = 0;
			int num = arr[i];
			while(num!=0){
				int fact = 1;
				for(int j = 1 ; j<=num%10 ; j++){
					fact *= j;
				}
				temp += fact;
				num /=10;
			}
			if(temp == arr[i])
				return i;
		}
		return -1;
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret = StrongNo(arr);
		if(ret !=-1){
			System.out.println("Strong no "+arr[ret]+" found at index "+ret);
		}else{
			System.out.println("Strong no not found");
		}
	}
}
