/*array 03
Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
*/

import java.io.*;

class ArrayDemo{
	void CountDigitArray(int arr[]){
		System.out.println("Digit count of Array elements");
		for(int i = 0 ; i<arr.length ; i++){
			int num = arr[i];
			int count = 0;
			while(num!=0){
				count++;
				num /= 10;
			}
			System.out.print(count+ " ");
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size : ");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		ArrayDemo obj = new ArrayDemo();

		obj.CountDigitArray(arr);
	}
}
