/*Program 9
Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255
*/

import java.io.*;

class ArrayDemo{

	static int secMin(int arr[]){
		int temp1 = arr[0];
		int temp2 = arr[0];
		for(int i = 0 ; i<arr.length ; i++){
			if(arr[i] < temp1){
				temp1 = arr[i];
			}
			if(arr[i]>temp2)
				arr[i] = temp2;
		}
		int sec = temp1;
		for(int i = 0 ; i<arr.length ; i++){
			if(arr[i]<temp2 && arr[i]>temp1)
				sec= arr[i];
		}
		return sec;
	}
			
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++)
			arr[i] = Integer.parseInt(br.readLine());

		System.out.println("Second min element : "+secMin(arr));

	}
}
