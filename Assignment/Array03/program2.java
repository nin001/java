/*Program 2
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/

import java.util.*;

class ArrayDemo{
	static void reverseElement(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int temp = 0;
			while(arr[i]!=0){
				temp = temp*10 + arr[i]%10;
				arr[i] /= 10;
			}
			arr[i] = temp;
		}
	}
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		
		System.out.println("Enter array Elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = sc.nextInt();
		}

		reverseElement(arr);

		System.out.println("Reversed elements array :");
		for(int i = 0 ; i<arr.length ; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
