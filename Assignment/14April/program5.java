//0
//1 1
//2 3 5
//(fibo)

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		int f1 = 0;
		int f2 = 1;
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				System.out.print(f1 + " ");
				int temp = f1;
				f1 = f2;
				f2 = temp+f2;
			}
			System.out.println();
		}
	}
}
