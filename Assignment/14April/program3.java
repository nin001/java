//54321
//8642
//963
//84
//5

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter row : ");
		int row = Integer.parseInt(br.readLine());
		int n =  row;
		for(int i = 1 ; i<=row ; i++){
			int mult = n;
			for(int j = 1 ; j<=row-i+1 ; j++){
				System.out.print(mult--*i + " ");
			}
			System.out.println();
			n--;
		}
	}
}
