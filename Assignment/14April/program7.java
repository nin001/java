//o
//14 13
//l  k  j
//9  8  7  6
//e  d  c  b  a

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		char ch = (char)((row*(row+1)/2)+64);
		int n  = row*(row+1)/2;

		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(row%2==0){
					if(i%2==0){
						System.out.print(ch + "\t");
					}else{
						System.out.print(n + "\t");
					}
				}else{
					if(i%2!=0){
						System.out.print(ch + "\t");
					}else{
						System.out.print(n + "\t");
					}
				}
				ch--;
				n--;
			}
			System.out.println();
		}
	}
}
