// print even in rev and odd as it is within given range

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter start : ");
		int st = Integer.parseInt(br.readLine());

		System.out.print("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		br.close();

		for(int i = end ; i>=st ; i--)
			if(i%2==0)
				System.out.print(i+" ");
		System.out.println();
		for(int i = st ; i<=end ; i++)
			if(i%2!=0)
				System.out.print(i+" ");
		System.out.println();
	}
}
