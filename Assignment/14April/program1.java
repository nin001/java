//D4 C3 B2 A1
//A1 B2 C3 D4
//D4 C3 B2 A1
//A1 B2 C3 D4

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		char ch = (char)(row+64);
		for(int i = 1 ; i<=row ; i++){
			int var = row;
			for(int j = 1 ; j<=row ; j++){
				if(i%2==0)
					System.out.print(ch++ + "" + j + " ");
				else
					System.out.print(ch-- + "" + var-- + " ");
			}
			System.out.println();
			if(i%2==0)
				ch--;
			else
				ch++;
		}
	}
}
