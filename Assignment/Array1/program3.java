// product of odd index only
import java.io.*;
class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int prod = 1;
		System.out.println("Enter elements");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 != 0 && arr[i]!=1)
				prod *= i;
		}
		System.out.println("Product : "+prod);
	}
}
