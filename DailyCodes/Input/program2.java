// Example in input

import java.util.Scanner;

class InputDemo{
	public static void main(String args[]){
		Scanner obj = new Scanner(System.in);

		System.out.println("Enter Number of Subjects : ");
		int num = obj.nextInt();

		int sum = 0;
		for(int i = 0 ; i<num ; i++){
			System.out.println("Enter Subject "+(i+1)+" Marks :");
			int marks = obj.nextInt();
			sum += marks;
		}

		System.out.println("Average marks : "+(sum/num));
	}
}
