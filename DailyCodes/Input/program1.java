//Input can be taken in java using two predefined classes 
//method 1 - Using scanner class
//method 2 - Using Buffer reader class and InputSteamReader class

import java.util.Scanner;

class InputDemo{
	public static void main(String args[]){
		Scanner obj = new Scanner(System.in);

		System.out.print("Enter InputString : ");
		String str1 = obj.next();
		System.out.println("Entered String : "+str1);
	}
}

/*When we input space between the string then it is not considered and the next string is skipped*/

/*
 Output when Util package is not included / Imported
 program1.java:7: error: cannot find symbol
		Scanner obj = new Scanner(System.in);
		^
  symbol:   class Scanner
  location: class InputDemo
program1.java:7: error: cannot find symbol
		Scanner obj = new Scanner(System.in);
		                  ^
  symbol:   class Scanner
  location: class InputDemo
2 errors
*/
