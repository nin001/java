// Method-2 Using buffer reader 

import java.io.*;

class InputDemo{
	public static void main(String args[])throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);

		BufferedReader br = new BufferedReader(isr);

		System.out.println("Enter Wing Name : ");
		char name1 =(char) br.read();
		
		br.skip(1);  // skips /n present in the buffer
		System.out.println("Enter Society Name : ");
		String name2 = br.readLine();

		
		System.out.println("Enter Your Name : ");
		String name3 = br.readLine();
		
		System.out.println(name1);
		System.out.println(name2);
		System.out.println(name3);
	}
}

/*
 * When we use read function before readline or other function
 * Due to stored "\n" in the buffer next readline takes "\n" as the input which is end of input for readline
 * that is the reason it gets skipped
*/
