// String tokenizer class 
// This class helps tokenizing input i.e. breaking dowm the string with some delimiter
// This class can be used when there is ask of taking input in single line

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.StringTokenizer;
import java.io.IOException;

class InputDemoStringTokenizer{
	public static void main(String args[])throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter Society name,wing,flatNo seperated by space");
		String info = input.readLine();

		StringTokenizer st = new StringTokenizer(info," ");

		String t1 = st.nextToken();
		String t2 = st.nextToken();
		String t3 = st.nextToken();

		System.out.println("Society Name : "+t1);
		System.out.println("Wing : "+t2);
		System.out.println("Flat no : "+t3);

	}
}	
