// integer float and double input using buffer class method

import java.io.*;

class InputDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter int :");
		int var1 = Integer.parseInt(br.readLine());

		System.out.println("Enter Float :");
		float var2 = Float.parseFloat(br.readLine());

		System.out.println("Enter intDouble :");
		double var3 = Double.parseDouble(br.readLine());

		System.out.println(var1);
		System.out.println(var2);
		System.out.println(var3);
	}
}

/*
 ".parse<dataType>
 is Wrapper class which is used to convert the String to Desired data type
 Here Wrappter class is used to convert the string input taken from readLine() method
 needs to be converted the the Int,Float etc data type so that compatible data gets in the 
 variable
*/
