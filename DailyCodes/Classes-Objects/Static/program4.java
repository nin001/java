//static methods
//can use class methods and variables direcly 
//but cannot use instance methods and variables without creating objects

class StaticDemo {
	int x = 10;
	static int y = 20;

	void fun() {
		System.out.println(x);
		System.out.println(y);
	}

	static void gun() {
		StaticDemo obj = new StaticDemo();
		System.out.println(obj.x);
		obj.fun();
	}

	public static void main(String args[]) {
		gun();
	}
}
