// static methods 
// static methods are methods that can be accessed using class name
// class name represents the speacial structure which consists addresses of 
// all static methods

class StaticDemo {
	static int x = 10;

	static void fun() {
		System.out.println(x);
	}

	public static void main(String args[]) {
		StaticDemo.fun();
	}
}
