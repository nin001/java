// static block
//
// static block only comes in picture when there exist static variables
// which is initialized inside the block by compiler
//
// if there doesnt exist 
// static block can be declared expilicitly by user

class StaticBlock {
	static {
		System.out.println("In static block1 ");
	}

	static {
		System.out.println("In static block2 ");
	}

	public static void main(String args[]) {
		System.out.println("In main");
	}

	static {
		System.out.println("In static block 3");
	}
}

/*
 * Multiple static block are merged as one sequencially
*/


