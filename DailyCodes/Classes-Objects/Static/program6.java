// Static block execution in 2 classes


class Demo {
	static {
		System.out.println("In Demo Static 1");
	}

	static {
		System.out.println("In Demo Static 2");
	}
}

class Client {
	static {
		System.out.println("In Cliend Static 1");
	}

	public static void main(String args[]) {
		Demo obj = new Demo();
		System.out.println("In main");
	}

	static {
		System.out.println("In Client Static 2");
	}
}

/*
In Cliend Static 1
In Client Static 2
In Demo Static 1
In Demo Static 2
In main
*/
