// Static 
//
// consists these
// static variables - which are global static and also called class variables and are initialized in 
// static block
//
// static block - static variables are initialized in static block which is present in method area.
// static block goes as stack frame even before main function and initialized all the static variables
//
// static methods - class methods consisting static keyword , these methods addresses are stored inside
// speacial structure of that class
//
// main - a static method which is implicitly called by jvm and we dont need to call main function/method

class StaticDemo {

	//static variables
	static int x = 10;

	static {
		System.out.println(x);
	}

	public static void main(String args[]) {
		System.out.println("In main");
	}
}
