// Static variable declaration

class StaticDemo {
	static int x = 10;

	static {
		static int y = 20;
	}

	void fun() {
		static int z = 15;
	}

	public static void main() {
		static int a = 30;
	}
}

/*
program2.java:7: error: illegal start of expression
		static int y = 20;
		^
program2.java:10: error: class, interface, or enum expected
	void fun() {
	^
program2.java:12: error: class, interface, or enum expected
	}
	^
program2.java:14: error: class, interface, or enum expected
	public static void main() {
	              ^
program2.java:16: error: class, interface, or enum expected
	}
	^
*/


/*Reason for not allowing static inside any block
 1. Static variables priority is the highest java has set who will be initialized or brought in picture
 first.
 2. Java supports only Global static variables but if we write/initialize it in block then it is not
 accessible.
 */


