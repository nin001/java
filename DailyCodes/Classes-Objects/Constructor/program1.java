// Constructor
// It is a speacial method which initializes the instance variable (non static)
// Constructor has no return type
// Constructor if we do not write , javac compiler adds a constructor in our code
// which has bytecode that is "invokeSpecial" that means Call the parents constructor
// In java those who do not have any Parent class their parent is Object class , Which is
// direct or indirect parent of the class

class ClsObj {
	
}

/*
 * Compiled from "program1.java"
class ClsObj {
  ClsObj();            -------this is constructor which is added by compiler while compilation
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V    --invokespecial which calls
       4: return                                                                      Object classes constructor
}
*/
