// constructor 
// programmer defined

class ConstructorDemo {
	ConstructorDemo() {
		System.out.println("In constructor 1");
	}

	ConstructorDemo(int x) {
		System.out.println("In constructor 2 value of x : "+x);
	}

	public static void main(String args[]) {
		ConstructorDemo obj1 = new ConstructorDemo();
		ConstructorDemo obj2 = new ConstructorDemo(10);
	}
}
