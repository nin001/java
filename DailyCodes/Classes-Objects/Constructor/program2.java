// non static variables are stored in constructor when object is created 
// inside object in the heap there is constructor space there are instance variables stored
//
// static variable / Class Variable
// These variables are stored in static block


class ClsObj {
	int var1 = 10;		//instance variable
	static int var2 = 30;

}

/*Reason to not allow non static variable to access from static context
 * BECAUSE non static variable/methods get there space/memory only after object is created */

/*
class ClsObj {
  int var1;         ---- instance variable declared here because it is accessible to every method in class

  static int var2;	---static/class variable declared here

  ClsObj();          ---constuctor 
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10	----- variable var1 stored inside constructor
       7: putfield      #2                  // Field var1:I
      10: return

  static {};		-- static block;
    Code:
       0: bipush        30               ----stored inside static block
       2: putstatic     #3                  // Field var2:I
       5: return
}
