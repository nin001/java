// Instance 
//
// Constists 
// Instance Variable - 
// Instance variable are global non-static variables which are initialized by Constructor
//
// Instance Block -
// Instance block is a block which is Excecuted when created an instance of the class
//
// Instance Methods - 
// Methods which do not have 'static' keyword
// These methods are present in method areas and their addresses are present in Object
//
// Constructor
// It is a non static speacial method which is implicitly called when we create an Instance
// of class


//IMPORTANT
//instance block are placed above the constructor code and below the initialization of instance var
//inside the constructor


class InstanceDemo {
	int x = 10;   //-- instance variable

	static int y = 20;
	
	static void gun() {
		System.out.println("in gun");
	}

	void fun() {
		int z = 30; 		//-- local variables
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}

	public static void main(String args[]) {
		InstanceDemo obj = new InstanceDemo();

		obj.fun();
	}
}
