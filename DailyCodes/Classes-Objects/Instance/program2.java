// instance block

class InstanceBlock {
	{
		System.out.println("Instance block1");
	}

	InstanceBlock() {
		System.out.println("In constructor");
	}

	{
		System.out.println("Instance block2");
	}
	public static void main(String args[]) {
		InstanceBlock obj = new InstanceBlock();
		System.out.println("in main");
	}
	{
		System.out.println("Instance block3");
	}
}

/* BYTE CODE OF CONSTRUCTOR
 InstanceBlock();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #3                  // String Instance block1
       9: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      12: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      15: ldc           #5                  // String Instance block2
      17: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      20: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      23: ldc           #6                  // String Instance block3
      25: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      28: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      31: ldc           #7                  // String In constructor
      33: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      36: return
*/

