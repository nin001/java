// Real time example

import java.io.*;

class Core2Web_JavaBatch2023 {
	int groupMembersCount;
	String groupName;
	int noOfAssignmentsGiven;
	int noOfAssignmentsCompleted;
	
	int flag = 0;

	void analytics()throws IOException{
		if(flag == 1){
			if(noOfAssignmentsGiven == noOfAssignmentsCompleted) {
				System.out.println("Group Has Completed Every Task");
				System.out.println("Great Work by Members and leader");
			}else if((noOfAssignmentsGiven/noOfAssignmentsCompleted) == 0) {
				System.out.println("Group Has Completed Good amount of Tasks");
				System.out.println("Good Work by Members and leader");
			}else {
				System.out.println("Group Has not Completed Good amount of Tasks");
				System.out.println("Need more Work by Members and leader");
			}
		}else{
			System.out.println("Kindly insert Values First\n");
			insertData();
		}
	}

	void insertData()throws IOException{
		flag = 1;
		BufferedReader br = new BufferedReader(new InputStreamReader (System.in));
		System.out.println("Enter number of Group members : ");
		groupMembersCount = Integer.parseInt(br.readLine());

		System.out.println("Enter Group Name : ");
		groupName = br.readLine();

		System.out.println("Enter no of Assingments Given : ");
		noOfAssignmentsGiven = Integer.parseInt(br.readLine());
		
		System.out.println("Enter no of Assingments Completed : ");
		noOfAssignmentsCompleted = Integer.parseInt(br.readLine());
	}

}


class SerializableGroupC2W {
	public static void main(String args[] )throws IOException{
		Core2Web_JavaBatch2023 grp = new Core2Web_JavaBatch2023();

		grp.insertData();
		grp.analytics();
	}
}
