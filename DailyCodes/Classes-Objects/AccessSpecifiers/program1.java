// There are four types of access specifiers
// 1. Default Specifiers - If we dont give any type of access specifier before any variables or methods then 
// it is of default type, default variables and methods are accessbile wihtin the folder , it can be accessed over
// multiple files but they need to be in same folder
//
// 2. Public Specifier - If we use keyword public before any variable , class or method then it is accessible from
// everywhere we just need to import the package
//
// 3. Private Specifier - If we use private before then that entity is only accessible within the class only
//
// 4. Protected


class JavaDemo {
	int x = 10;
	String str = "Niraj";

	private int z = 20;

	void disp() {
		System.out.println(x);
		System.out.println(str);
		System.out.println(z);
	}
}

class MainDemo {
	public static void main(String args[]) {
		JavaDemo obj = new JavaDemo();

		obj.disp();

		System.out.println(obj.x);
		System.out.println(obj.str);

//		System.out.println(obj.z);		//error 
							//z has private access in JavaDemo
	}
}

/*
program1.java:36: error: z has private access in JavaDemo
		System.out.println(obj.z);*/
