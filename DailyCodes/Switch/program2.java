// Character and ascii value 

class SwitchDemo{
	public static void main(String args[]){
		int ch = 65;
		switch(ch){
			case 'A':
				System.out.println("char "+ch);
				break;
			case 65:
				System.out.println("num "+ch);
				break;
			default:
				System.out.println("invalid");
		}
	}
}

/*
 program2.java:10: error: duplicate case label
			case 65:
			^
1 error
*/

