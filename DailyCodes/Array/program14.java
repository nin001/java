// Internals of array

class ArrayDemo{
	public static void main(String args[]){
		int arr[] = {10,20,30,40};

		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));

		ArrayDemo obj = new ArrayDemo();
//function that changes the values of arr[1]and[2] within range of -128 to 127
		obj.fun(arr);

		int x = 70;
		int y = 80;

		System.out.println("Array [1] : "+System.identityHashCode(arr[1]));
		System.out.println("Variable x: "+System.identityHashCode(x));

		System.out.println("Array [2] : "+System.identityHashCode(arr[2]));
		System.out.println("Variable y: "+System.identityHashCode(y));
	}

	static void fun(int arr[]){
		arr[1] = 70;
		arr[2] = 80;
	}
}

/*
 Array [1] : 1836905146
Variable x: 1836905146
Array [2] : 598802657
Variable y: 598802657*/

