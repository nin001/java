// Array in java
// Array is a concept which is used to store same type of datatype under same variable
// Array declaration in java can be done by "int arr[]" this doesnt allocate space but arr[] array is been declared
// To assign memory in java is done through new 
// Array in java is dynamic , stored in heap 

class ArrayDemo{
	public static void main(String args[]){
		int arr[];     //-->this statement shows error in c (array size missing)
		int arr1[] = new int[5];   // method 1 to initialize array where there is array made in heap section
					   // where values if not assigned are 0 as default
		//int arr[] = new int[];-->compile time error -(array dimensions missing)

	}
}
