
import java.util.Scanner;

class Array2D{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter row :");
		int row = input.nextInt();

		System.out.println("Enter col : ");
		int col = input.nextInt();

		int arr[][] = new int[row][col];

		System.out.println("Enter array elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			for(int j = 0 ; j<arr[i].length ; j++){
				arr[i][j] = input.nextInt();
			}
		}

		System.out.println("2D Array");
		for(int i = 0 ; i<arr.length ; i++){
			for(int j = 0 ; j<arr[i].length ; j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}
