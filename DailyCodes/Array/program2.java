// Assignment of elements to array

import java.io.*;

class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr1[] = new int[]{10,20,30,40,50};      //method 1
		int arr2[] = {10,20,30,40,50};               //This statement is same as previous
		
		System.out.println("Array 1 : ");
		for(int i = 0 ; i<5 ; i++){
			System.out.print(arr1[i] + " ");
		}

		System.out.println("\nArray 2 :");
		for(int i = 0 ; i<5 ; i++){
			System.out.print(arr2[i] + " ");
		}
		System.out.println();
		
		int arr[] = new int[5];

		System.out.println("Enter values of Array 3 :");
		for(int i = 0 ; i<5 ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("\nArray 3 :");
		for(int i = 0 ; i<5 ; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
