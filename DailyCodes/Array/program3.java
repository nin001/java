// Arrays can be of multiple data type 
// Primitive data types : int , float , char , boolean
import java.io.*;
class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size of Arrays : ");
		int size = Integer.parseInt(input.readLine());
		
		int arr1[] = new int[size];
		char arr2[] = new char[size];
		float arr3[] = new float[size];
		boolean arr4[] = new boolean[size];

		System.out.println("Enter Integer Data : ");
		for(int i = 0 ; i<size ; i++){
			arr1[i] = Integer.parseInt(input.readLine());
		}
		System.out.println("Enter Char Data : ");
		for(int i = 0 ; i<size ; i++){
			arr2[i] = input.readLine().charAt(0);
		}
		System.out.println("Enter Float Data : ");
		for(int i = 0 ; i<size ; i++){
			arr3[i] = Float.parseFloat(input.readLine());
		}
		System.out.println("Enter Boolean Data : ");
		for(int i = 0 ; i<size ; i++){
			arr4[i] = Boolean.parseBoolean(input.readLine());
		}

		System.out.println("Array1:");
		for(int i = 0 ; i<size ; i++){
			System.out.println(arr1[i]);
		}
		System.out.println("Array2:");
		for(int i = 0 ; i<size ; i++){
			System.out.println(arr2[i]);
		}
		System.out.println("Array3:");
		for(int i = 0 ; i<size ; i++){
			System.out.println(arr3[i]);
		}
		System.out.println("Array4:");
		for(int i = 0 ; i<size ; i++){
			System.out.println(arr4[i]);
		}

	}
}

/*
Enter Size of Arrays : 
5
Enter Integer Data : 
10.5                      -->CANNOT ASSIGN DOUBLE VALUES TO THE INTEGER DATA TYPE
Exception in thread "main" java.lang.NumberFormatException: For input string: "10.5"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at ArrayDemo.main(program3.java:18)
niraj@niraj-IdeaPad:~/java-dsa/DailyCodes/Array$ java ArrayDemo 
Enter Size of Arrays : 
5
Enter Integer Data :
a			---> CANNOT ASSIGN CHARACTER TO THE INTEGER
Exception in thread "main" java.lang.NumberFormatException: For input string: "a"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at ArrayDemo.main(program3.java:9)
*/
