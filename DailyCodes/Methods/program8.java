// boolean data type

class MethodDemo{
	public static void main(String args[]){
		System.out.println("Boolean datatype");
		fun(true);
		fun(false);
//		fun(10);
//		fun(0);
//		fun('A');
	}

	static void fun(boolean b){
		System.out.println("boolean value : " + b);
	}
}
/*
program8.java:8: error: incompatible types: int cannot be converted to boolean
		fun(10);
		    ^
program8.java:9: error: incompatible types: int cannot be converted to boolean
		fun(0);
		    ^
program8.java:10: error: incompatible types: char cannot be converted to boolean
		fun('A');
*/
