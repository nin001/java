// argument and parameter data types different

class MethodDemo{
	static void fun(float x){
		System.out.println("In Fun");
		System.out.println(x);
	}

	public static void main(String args[]){
		System.out.println("In main");
		fun(10);
		//fun(10.5);  	Double cannot be converted to float "Incompatible types : Possible lossy convn"
		fun(10.5f);
		fun('A');
	}	
}

