// pellindrome
import java.util.*;
class MethodDemo{
	static boolean isPalindrom(int a){
		int pal = 0;
		int temp = a;
		while(a!=0){
			pal = pal*10 + (a%10);
			a /= 10;
		}
		if(temp == pal)
			return true;
		else
			return false;
	}

	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter number : ");
		int num = input.nextInt();

		if(isPalindrom(num)){
			System.out.println(num + " is palindrom number ");
		}else{
			System.out.println(num + " is not palindrom number ");
		}
	}
}
