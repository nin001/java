// reverse number

import java.io.*;

class MethodDemo{

	static int reverse(int num){
		int rev = 0;
		while(num!=0){
			rev = rev*10 + (num%10);
			num /= 10;
		}
		return rev;
	}
	public static void main(String args[])throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter number :");
		int num = Integer.parseInt(input.readLine());

		int rev = reverse(num);
		
		System.out.println("Reversed Number : "+rev);
	}
}
