// what is the reason to write the main(entry point function as public static void main(String[] args)
// -the static keyword in the psvm is that the any method or varible that are declared static are accesible
// without creating object this is done for making jvms work easy so that jvm not need to make object of class
// in order to access main function
// - The public keyword is that the classes method main is accessible for every one 
// - Void is the return type of the main function it is put void because it doesnt need to return anything to jvm

class Demo{
	public static void main(String args[]){
		System.out.println("Hello World");
	}
}

// basic code of java where String,System are classes that are library classes(predefined)
// the sequence of public static doesnt matter and the array'[]' declaration sequence doesnt matter
// can be String[] args
// and also String args[]
//
// and instead of args as is a varible name we can write anything 
// such as String nin[]
