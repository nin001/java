// hashcode demo

class HashcodeDemo{
	public static void main(String args[]) {
		String str = "Niraj";
		String str2 = new String("Niraj");
		String str3 = "Niraj";

		System.out.println(str.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
	}
}
