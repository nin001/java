// String is a data type which stores Set of characters in it
// In java String is a class
// Initialisation of String

class StringDemo{
	public static void main(String args[]) {
		String str1 = "Niraj";
		String str2 = new String("Niraj");
		char str3[] = {'N','i','r','a','j'};

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
