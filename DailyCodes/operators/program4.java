// unary 3

class unary{

	public static void main(String[] args){
		int x = 6;
		int y = 8;

		System.out.println(++x);
		System.out.println(++y);

		System.out.println(x++);
		System.out.println(y++);

		System.out.println(--x);
		System.out.println(--y);

		System.out.println(x--);
		System.out.println(y--);

		System.out.println(x);
		System.out.println(y);
	}
}
