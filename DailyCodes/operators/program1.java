//java operators
/*
 Operator is the action that is going to be performed on respective operand/operands
 Types of operator
 1. Arithmetic - +,-,/,*,%
 2. Relational - <,>,<=,>=,!=,==
 3. Logical operator - && || ^
 4. Unary operator - ++,--,+,-
 */

class operator{
	public static void main(String[] args){
		int x = 10;
		int y = 20;

		System.out.println(x+y);
		System.out.println(x-y);
		System.out.println(x/y);
		System.out.println(x*y);
		System.out.println(x%y);
	}
}
